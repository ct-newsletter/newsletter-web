import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { PostService } from 'src/app/core/services/post.service';
import { GetAllPostsRes, Post } from 'src/app/core/models/post';
import { UserService } from 'src/app/core/services/user.service';
import { UserType } from 'src/app/core/models/user';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  public posts: Post[] = [];
  public isLoading: boolean;
  public hasErrored: boolean;
  public emptyPosts: boolean;

  constructor(
    private postService: PostService,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) { }

  ngOnInit() {
    this.getAllPosts();
  }

  private getAllPosts(): void {
    this.isLoading = true;
    this.hasErrored = false;
    this.emptyPosts = false;

    this.postService.getAllPosts()
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: this.handleGetAllPosts.bind(this),
        error: this.handleGetAllPostsError.bind(this),
      });
  }

  private handleGetAllPosts(res: GetAllPostsRes): void {
    this.posts = res.data;
    this.emptyPosts = !(res && res.data && res.data.length);
  }

  private handleGetAllPostsError(): void {
    this.hasErrored = true;
    this.emptyPosts = true;
  }

  public handleDeletePost(PostUID: string): void {
    this.postService.deletePost({ PostUID })
      .subscribe({
        next: this.handlePostDelete.bind(this),
        error: this.handleDeletePostError.bind(this)
      });
  }

  private handlePostDelete(): void {
    this.postService.getAllPosts()
      .subscribe({
        next: this.handleGetAllPosts.bind(this),
        error: this.handleDeletePostError.bind(this),
      });
  }

  private handleDeletePostError(): void {
    this.snackbarService.error('This post is probably not yours.');
  }

  public get isAdmin(): boolean {
    const userData = this.userService.userData;
    return userData && userData.IsAdmin;
  }

  public isAuthor(): boolean {
    return this.userService.isAuthor;
  }

  public isAuthorAndPostOwner(authorUID: string): boolean {
    return this.userService.isAuthorAndPostOwner(authorUID);
  }
}
