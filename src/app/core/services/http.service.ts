import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { NewsletterError } from 'src/app/shared/models/error';

@Injectable({
    providedIn: 'root'
})
export class HttpService {
    constructor(
        private http: HttpClient,
        private authService: AuthService,
    ) { }

    public readonly apiUrl = environment.apiUrl;

    post(apiPath: string, data: any = null, isJson: boolean = true): Observable<any> {
        const newData = isJson ? this.buildApiData(data) : data;
        return this.http
            .post<any>(this.buildApiUrl(apiPath), newData, this.headers)
            .pipe(catchError(this.handleError.bind(this)));
    }

    handleError(error: HttpErrorResponse | any): Observable<any> {
        if (error && error.status === 401) {
            this.authService.logout();
        }

        return throwError(new NewsletterError(error));
    }

    get headers(): any {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: this.getAuthToken()
            })
        };
    }

    private getAuthToken(): string {
        return this.authService.AuthToken || '';
    }

    private buildApiUrl = (apiPath: string) => this.apiUrl + apiPath;
    private buildApiData = (data: any) => JSON.stringify(data);
}
