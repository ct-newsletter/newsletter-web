export interface Post {
    PostUID: string;
    CreatedAt: string;
    UpdatedAt: string;
    Title: string;
    AuthorName: string;
    Content: string;
}

export interface CreatePostReq {
    AuthorUID: string;
    Title: string;
    Content: string;
}

export interface GetPostReq {
    PostUID: string;
}

export interface GetAllPostsReq {
    SearchTerm?: string;
    RowOffset?: number;
    RowLimit?: number;
}

export interface GetAllPostsRes {
    data: Post[];
    meta: GetAllPostsReq;
}

export interface EditPostReq {
    PostUID: string;
    Title: string;
    Content: string;
}

export interface EditPostRes {
    UpdatedAt: string;
}

export interface DeletePostReq {
    PostUID: string;
}
