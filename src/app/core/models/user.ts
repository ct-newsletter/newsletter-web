export enum UserType {
    Subscriber = 0,
    Author = 1,
}

export interface User {
    UserUID: string;
    Username: string;
    FirstName: string;
    LastName: string;
    UserType: UserType;
    IsAdmin: boolean;
}

export interface GetUserReq {
    UserUID: string;
}

export interface GetUserRes {
    UserUID: string;
    Username: string;
    FirstName: string;
    LastName: string;
    UserType: number;
    IsAdmin: boolean;
}

export interface GetAllUsersReq {
    SearchTerm?: string;
    RowOffset?: number;
    RowLimit?: number;
}

export interface GetAllUsersRes {
    Data: User[];
    Meta: GetAllUsersReq;
}

export interface EditUserReq {
    UserUID: string;
    Username: string;
    FirstName: string;
    LastName: string;
    UserType: UserType;
    IsAdmin: boolean;
}

export interface DeleteUserReq {
    UserUID: string;
}
