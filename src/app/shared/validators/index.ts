import { FormControl } from '@angular/forms';

export class AppValidators {
    /**
     * Validate that control value is not spaces only
     *
     * @param control Form control
     */
    public static noWhitespace(control: FormControl): any {
        const isWhitespace =
            control.value &&
            control.value.length > 0 &&
            control.value.trim().length === 0;
        return isWhitespace ? { whitespace: true } : null;
    }
}
