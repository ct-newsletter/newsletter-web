import { Theme } from 'src/themes/theme';

export const environment = {
  production: true,
  dateTimeFormat: 'd. M. yyyy. \'at\' H:mm',
  apiUrl: 'http://192.168.0.24:8080/api',
  version: '0.1',
  theme: Theme.Default
};

