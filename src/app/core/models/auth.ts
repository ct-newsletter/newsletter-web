import { UserType } from 'src/app/core/models/user';

export interface RegisterAccountReq {
    Username: string;
    FirstName: string;
    LastName: string;
    UserType: UserType;
}

export interface AuthReq {
    Username: string;
}

export interface AuthRes {
    AuthToken: string;
}
