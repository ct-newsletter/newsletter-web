import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { CreatePostReq, GetPostReq, GetAllPostsReq, GetAllPostsRes, Post, EditPostReq, EditPostRes, DeletePostReq } from '../models/post';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PostService {
    private readonly api = {
        create: '/post/create',
        get: '/post/get',
        getAll: '/post/getAll',
        edit: '/post/edit',
        delete: '/post/delete',
    };

    constructor(
        private httpService: HttpService
    ) { }

    createPost(request: CreatePostReq): Observable<Post> {
        return this.httpService.post(this.api.create, request);
    }

    getPost(request: GetPostReq): Observable<Post> {
        return this.httpService.post(this.api.get, request);
    }

    getAllPosts(request?: GetAllPostsReq): Observable<GetAllPostsRes> {
        return this.httpService.post(this.api.getAll, request);
    }

    editPost(request: EditPostReq): Observable<EditPostRes> {
        return this.httpService.post(this.api.edit, request);
    }

    deletePost(request: DeletePostReq): Observable<any> {
        return this.httpService.post(this.api.delete, request);
    }
}
