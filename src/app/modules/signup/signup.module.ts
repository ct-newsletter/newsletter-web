import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { SharedModule } from 'src/app/shared/shared.module';
import { SignupRoutingModule } from './signup-routing.module';

// Components
import { SignupComponent } from './components/signup/signup.component';


@NgModule({
  declarations: [SignupComponent],
  imports: [
    CommonModule,
    SharedModule,
    SignupRoutingModule
  ]
})
export class SignupModule { }
