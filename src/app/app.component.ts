import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './core/services/auth.service';
import { UserService } from './core/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'newsletter-web';

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
  ) { }

  // We check if there is Auth Token inside local storage
  // if not we proceed to login
  // if there is a token we redirect to posts and
  // if auth token expired we will get 401 from the server
  // and log out will happen inside http.service
  public ngOnInit(): void {
    if (!this.authService.AuthToken) {
      this.router.navigate(['/login']);
    }

    this.getAndSaveUserData();
    this.subscribeToAuthChanges();
  }

  // Listen when a user logs in or logs out
  private subscribeToAuthChanges(): void {
    this.authService.onAuthEvent().subscribe({
      next: this.handleAuthValue.bind(this)
    });
  }

  private handleAuthValue(isAuthenticated: boolean): void {
    if (isAuthenticated) {
      this.router.navigate(['/posts']);
      return;
    }

    this.router.navigate(['/login']);
  }

  private getAndSaveUserData(): void {
    this.userService.getAndSaveUserData(this.authService.AuthToken).subscribe();
  }
}
