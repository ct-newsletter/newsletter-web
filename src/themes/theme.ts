export enum Theme {
    Default = 'default',
    Afife = 'afife',
    AylenMilla = 'aylenmilla',
}

export const Themes = {
    [Theme.Default]: {
        locale: 'en',
    },
    [Theme.Afife]: {
        locale: 'es',
    },
    [Theme.AylenMilla]: {
        locale: 'es',
    }
};
