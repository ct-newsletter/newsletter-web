import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { PostService } from 'src/app/core/services/post.service';
import { AppValidators } from 'src/app/shared/validators';
import { AuthService } from 'src/app/core/services/auth.service';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  public isLoading = false;
  public form: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private postService: PostService,
    private snackbarService: SnackbarService,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  public handleSubmit(): void {
    this.isLoading = true;
    this.postService.createPost(this.form.value)
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: this.handleCreatePost.bind(this),
        error: this.handleCreatePostError.bind(this),
      });
  }

  private handleCreatePost(): void {
    this.snackbarService.success('Post created successfully.');
    this.router.navigate(['/posts']);
  }

  private handleCreatePostError(error: any): void {
    this.snackbarService.error('Failed to create new post.');
  }

  private createForm(): void {
    this.form = this.fb.group({
      AuthorUID: this.authService.AuthToken,
      Title: [
        '',
        [
          Validators.required,
          AppValidators.noWhitespace,
        ],
      ],
      Content: [
        '',
        [
          Validators.required,
        ]
      ]
    });
  }

}
