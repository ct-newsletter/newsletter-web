import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { UserService } from 'src/app/core/services/user.service';
import { AppValidators } from 'src/app/shared/validators';
import { AuthService } from 'src/app/core/services/auth.service';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public hide = true;
  public isLoading = false;
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  public handleSubmit(): void {
    this.isLoading = true;
    this.userService.login(this.form.value)
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        error: this.handleLoginError.bind(this),
      });
  }

  private handleLoginError(error: any): void {
    this.snackbarService.error('Username is wrong.');
  }

  private createForm(): void {
    this.form = this.fb.group({
      Username: [
        '',
        [
          Validators.required,
          AppValidators.noWhitespace,
        ],
      ],
      Password: ['']
    });
  }
}
