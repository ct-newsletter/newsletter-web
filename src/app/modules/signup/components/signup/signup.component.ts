import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { UserService } from 'src/app/core/services/user.service';
import { AppValidators } from 'src/app/shared/validators';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public hide = true;
  public isLoading = false;
  public form: FormGroup;
  public userTypes = [{ value: 0, label: 'Subscriber' }, { value: 1, label: 'Author' }];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private userService: UserService,
    private snackbarService: SnackbarService,
  ) {
  }

  ngOnInit() {
    this.createForm();
  }

  public handleSubmit(): void {
    this.isLoading = true;
    this.userService.registerUser(this.form.value)
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: this.handleSingUpSuccess.bind(this),
        error: this.handleSignUpError.bind(this),
      });
  }

  private handleSingUpSuccess(): void {
    this.snackbarService.success('User created successfully.');
    this.router.navigate(['/login']);
  }

  private handleSignUpError(error: any): void {
    this.snackbarService.error('Failed to create user.');
  }

  private createForm(): void {
    this.form = this.fb.group({
      Username: [
        '',
        [
          Validators.required,
          AppValidators.noWhitespace,
        ],
      ],
      FirstName: [
        '',
        [
          Validators.required,
          AppValidators.noWhitespace,
        ],
      ],
      LastName: [
        '',
        [
          Validators.required,
          AppValidators.noWhitespace,
        ],
      ],
      UserType: [
        0,
        [
          Validators.required
        ]
      ]
    });
  }
}
