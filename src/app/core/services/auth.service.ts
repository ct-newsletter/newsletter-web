import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private authToken: string;
    private isAuthenticated$: Subject<boolean> = new Subject<boolean>();

    constructor() {}

    get AuthToken(): string {
        return this.authToken || (this.authToken = localStorage.getItem('NW-AuthToken'));
    }

    set AuthToken(val: string) {
        if (!val) {
            localStorage.removeItem('NW-AuthToken');
        } else {
            this.authToken = val;
            localStorage.setItem('NW-AuthToken', val);
        }
    }

    public logout(): void {
        this.AuthToken = null;
        this.isAuthenticated$.next(false);
    }

    public login(authToken: string): void {
        this.AuthToken = authToken;
        this.isAuthenticated$.next(true);
    }

    public isAuthenticated(): boolean {
        return !!this.AuthToken;
    }

    public onAuthEvent(): Observable<boolean> {
        return this.isAuthenticated$.asObservable();
    }
}
