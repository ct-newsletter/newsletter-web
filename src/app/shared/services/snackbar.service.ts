import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../components/snackbar/snackbar.component';


@Injectable({
    providedIn: 'root'
})
export class SnackbarService {
    constructor(private snackBar: MatSnackBar) { }

    public success(message?: string) {
        this.snackBar.openFromComponent(SnackbarComponent, {
            duration: 4000,
            data: {
                type: 'success',
                message,
            },
        });
    }

    public error(message?: string) {
        this.snackBar.openFromComponent(SnackbarComponent, {
            duration: 4000,
            data: {
                type: 'error',
                message,
            },
        });
    }
}
