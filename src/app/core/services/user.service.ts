import { Injectable } from '@angular/core';
import { Observable, Subscriber, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { HttpService } from './http.service';
import { RegisterAccountReq, AuthReq, AuthRes } from '../models/auth';
import { User, GetUserReq, GetAllUsersReq, GetAllUsersRes, EditUserReq, UserType } from '../models/user';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private readonly api = {
        auth: '/user/auth',
        create: '/user/create',
        get: '/user/get',
        getAll: '/user/getAll',
        edit: '/user/edit',
        delete: '/user/delete',
    };

    public userData: User;

    constructor(
        private httpService: HttpService,
        private authService: AuthService,
    ) { }

    login(request: AuthReq): Observable<boolean> {
        return this.httpService.post(this.api.auth, request)
            .pipe(
                flatMap(this.saveAuthToken.bind(this)),
                flatMap(this.getUserData.bind(this)),
                flatMap(this.saveUserData.bind(this)),
            );
    }

    private saveAuthToken(res: AuthRes): Observable<AuthRes> {
        this.authService.login(res.AuthToken);
        return of(res);
    }

    getUserData(req: AuthRes): Observable<User> {
        return this.getUser({ UserUID: req.AuthToken });
    }

    private saveUserData(res: User): Observable<boolean> {
        this.userData = res;
        return of(true);
    }

    getAndSaveUserData(UserUID: string): Observable<User> {
        return this.getUser({ UserUID })
            .pipe(flatMap(this.saveUserData.bind(this)));
    }

    registerUser(request: RegisterAccountReq): Observable<User> {
        return this.httpService.post(this.api.create, request);
    }

    getUser(request: GetUserReq): Observable<User> {
        return this.httpService.post(this.api.get, request);
    }

    getAllUsers(request: GetAllUsersReq): Observable<GetAllUsersRes> {
        return this.httpService.post(this.api.getAll, request);
    }

    editUser(request: EditUserReq): Observable<any> {
        return this.httpService.post(this.api.edit, request);
    }

    isPostOwner(authorUID: string): boolean {
        return this.userData && this.userData.UserUID === authorUID;
    }

    isAuthorAndPostOwner(authorUID: string): boolean {
        return this.isAuthor && this.isPostOwner(authorUID);
    }

    get isAdmin(): boolean {
        return this.userData && this.userData.IsAdmin;
    }

    get isAuthor(): boolean {
        return this.userData && this.userData.UserType === UserType.Author;
    }
}
