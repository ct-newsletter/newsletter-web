import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { Post } from 'src/app/core/models/post';
import { PostService } from 'src/app/core/services/post.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { AppValidators } from 'src/app/shared/validators';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
  public isLoading: boolean;
  public hasErrored: boolean;
  public post: Post;
  public form: FormGroup;

  private postUID: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private postService: PostService,
    private snackbarService: SnackbarService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.postUID = params.id;
      this.getPost(params.id);
    });
  }

  public handleSubmit(): void {
    this.isLoading = true;
    this.postService.editPost(this.form.value)
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: this.handleEditPost.bind(this),
        error: this.handleEditPostError.bind(this),
      });
  }

  private handleEditPost(): void {
    this.snackbarService.success('Updated post successfully.');
    this.router.navigate(['/posts']);
  }

  private handleEditPostError(error: any): void {
    this.snackbarService.error('This is probably not your post.');
    console.error(error);
  }

  private getPost(PostUID: string): void {
    this.isLoading = true;
    this.hasErrored = false;
    this.postService.getPost({ PostUID })
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: this.handleGetPost.bind(this),
        error: this.handleGetPostError.bind(this),
      })
  }

  private handleGetPost(res: Post): void {
    this.post = res;
    this.createForm(this.post);
  }

  private createForm(post: Post): void {
    this.form = this.fb.group({
      AuthorUID: this.authService.AuthToken,
      PostUID: this.postUID,
      Title: [
        post.Title,
        [
          Validators.required,
          AppValidators.noWhitespace,
        ],
      ],
      Content: [
        post.Content,
        [
          Validators.required,
        ]
      ]
    });
  }

  private handleGetPostError(): void {
    this.hasErrored = true;
  }

}
