import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Post } from 'src/app/core/models/post';
import { PostService } from 'src/app/core/services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  public isLoading: boolean;
  public hasErrored: boolean;
  public post: Post;
  public dateFormat = environment.dateTimeFormat;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.getPost(params.id);
   });
  }

  private getPost(PostUID: string): void {
    this.isLoading = true;
    this.hasErrored = false;
    this.postService.getPost({ PostUID })
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe({
        next: this.handleGetPost.bind(this),
        error: this.handleGetPostError.bind(this),
      })
  }

  private handleGetPost(res: Post): void {
    this.post = res;
  }

  private handleGetPostError(): void {
    this.hasErrored = true;
  }
}
