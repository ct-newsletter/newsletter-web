import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Modules
import { MaterialModule } from './modules/material.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { SnackbarComponent } from './components/snackbar/snackbar.component';

// Services
import { SnackbarService } from './services/snackbar.service';

@NgModule({
    declarations: [NavigationComponent, SnackbarComponent],
    providers: [SnackbarService],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        // Components
        NavigationComponent,
    ],
    entryComponents: [SnackbarComponent]
})
export class SharedModule { }